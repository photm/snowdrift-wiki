<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        $if(feed)$
        <link href="$base$/_feed/" type="application/atom+xml" rel="alternate" title="$wikititle$" />
        <link href="$base$/_feed$pageUrl$" type="application/atom+xml" rel="alternate" title="$wikititle$ - $pagetitle$" />
        $endif$
        <title>$wikititle$ - $pagetitle$</title>
        $if(printable)$
        <link href="$base$/css/print.css" rel="stylesheet" media="all" type= "text/css" />
        $else$
        <link href="$base$/css/custom.css" rel="stylesheet" media="screen, projection" type="text/css" />
        <link href="$base$/css/print.css" rel="stylesheet" media="print" type= "text/css" />
        $endif$
        <!--[if IE]><link href="$base$/css/ie.css" rel="stylesheet" media="screen, projection" type="text/css" /><![endif]-->
    </head>
    <body>
        <nav>$userbox()$</nav>
        <div class="container">
            <div class="mainCol">
                $tabs$
                $parents$
                $content()$
            </div>
            <nav class="sidebar">
                $logo()$
                $if(sitenav)$
                    $sitenav()$
                $endif$
                $if(pagetools)$
                    $pagetools()$
                $endif$
                $if(markuphelp)$
                    $markuphelp()$
                $endif$
            </nav>
        </div>
        <footer>$footer()$</footer>
        $javascripts$
        $expire()$
        $getuser()$
    </body>
</html>

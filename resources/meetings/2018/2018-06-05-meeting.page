<!-- Previous week's meeting notes: https://git.snowdrift.coop/sd/wiki/tree/master/resources/meetings/2018 -->

# Meeting — June 5, 2018

Attendees: wolftune, smichel17, msiep, Salt

<!-- Add agenda items below -->

## Meeting task capture
- smichel17: I don't want to capture other people's tasks. From Taiga experience, that leads to us not using the platform.
- smichel17: I do recognize that some hand-holding/advice from is helpful.
- smichel17: This could be done in backlog grooming meetings, but we're not doing those right now.
- smichel17: So, proposal: End the meeting 10 minutes earlier, give people a chance to capture their own todos in gitlab (or wherever).
- Those familiar with project management tools (eg, smichel17) can assist anyone less familiar
- NEXT STEP: Try this today [DONE]

## GDPR, privacy policy, related
- wolftune: smichel17 and I have been working on gdpr compliance (and generally improving/simplifying our privacy policy)
- wolftune: Current draft work is at https://snowdrift.sylphs.net:8002/p/privacy
- wolftune: We want the policy to reflect our values, not just be legally compliant.
- wolftune: Wanted to get quick feedback on what we were thinking about for civi: if we get person X's information from someone else, our first contact to X should ask for permission to continue storing their data (and we remove it if they ask us to or don't respond).
- Salt: that sounds good
- Salt: One site I saw had an email I really liked, to the effect of: "Our policy from the beginning has been to respect your privacy; we're not changing our policy in spirit, but we'd like to take this opportunity to look over our policy again."
- NEXT STEP: n/a

## looking forward: CLS, OSCON
- wolftune: <some stuff missed in the notes>
- wolftune: I'd like to set this as something of a milestone to get things in an acceptable shape by then
- Salt: I have lodging if I make it, though I don't need to since the panels I submitted didn't get accepted
- NEXT STEP: Identify and focus on anything we can finish in a 6-week timespan and communicate this time-frame to others
- NEXT STEP: announce that some of us will be at CLS https://git.snowdrift.coop/sd/outreach/issues/22

## Tracking/poking status on site development
- wolftune: While I'm trying to deal with one thing at a time, I know that design work has moved ahead a little / implementation can start.
- wolftune: Is there anything I or we can do to help push work forward (short of the big work in my mind — getting discourse fully up, blog posts published, etc)
- wolftune: I guess, just, any thoughts?
- others: <not really>
- wolftune: I guess just know that I have a plan for moving forward and revitalizing the team
- NEXT STEP: n/a

## Discourse categories (Feedback & Support)
- NEXT STEPS: read/reply https://community.snowdrift.coop/t/adding-a-feedback-support-category/800

----

## Carry-overs

<!-- Review status of carry-overs -->

<!-- Decide where to capture outstanding tasks (new & carryover) -->

```
## Ford/Sloan Grant (outreach repo, wolftune):
- NEXT STEP: submit application by deadline (June 13), assigned: wolftune https://git.snowdrift.coop/sd/outreach/issues/23
    - https://ford-foundation-6.forms.fm/digital-infrastructure-research-rfp/forms/4770

## CLS/OSCON (outreach repo, wolftune):
- NEXT STEP: Civi / Portland-list / social-media, plan announcements that we'll be at CLS etc. maybe a Snowdrift meetup
- [ ] Announce-list email / Discourse post in Announcements / social posts ? 

## Breaking things into tiny chunks, easy/short (governance repo, wolftune)
- NEXT STEP: Plan all the steps for developing grooming procedure
- NEXT STEP: develop process for grooming / regular (weekly?) review of GitLab issues etc., [wolftune] https://git.snowdrift.coop/sd/governance/issues/38
- NEXT STEP: collect a short static wiki page of "short things to do to help" https://community.snowdrift.coop/t/collecting-list-of-little-helpful-things-anyone-can-do/812
- NEXT STEP: make GitLab tags for: Urgent, Important, Quick, Easy https://community.snowdrift.coop/t/tagging-gitlab-issues-across-repos/813

## engaging on Discourse (outreach repo; Salt, but not today)
- NEXT STEP: write intros: https://git.snowdrift.coop/sd/outreach/issues/14
- NEXT STEP: post a discourse announcement about meetings status (including inviting people who aren't core contributors/team)… [wolftune/Salt]
- NEXT STEP: Salt opens Discourse topic on planning useful pinned topics (fpbot, etc)
- NEXT STEP: make a pinned post encouraging people to introduce themselves (some guidance about what sorts of things to mention)
- NEXT STEP: add pinned posts to welcome that are the guides to Discourse and community guideline links, FAQ…
- NEXT STEP: <https://community.snowdrift.coop/t/draft-please-join-us-on-the-snowdrift-community-forum/567>
- NEXT STEP: <https://community.snowdrift.coop/t/draft-welcome-to-the-snowdrift-coop-community/568>

## Project outreach steps (outreach repo)
- NEXT STEP: schedule meeting to work on kanban sorting for recruiting [Salt]

## 1-on-1 checkins (governance repo, wolftune) https://git.snowdrift.coop/sd/governance/issues/39
- NEXT STEP: wolftune will start to do this a bit
- NEXT STEP: wolftune will create a role for this (volunteer coordinator?)
- NEXT STEP: wolftune with look for someone to fill the role

## governance roles overhaul (governance repo, wolftune)
- NEXT STEP: wolftune will work on list of roles and defined responsibilities for accepting *any* role [too broad to capture directly, but overall issues are captured in various ways]

```

<!-- Capture TODOs -->

<!-- Add meeting notes to wiki -->
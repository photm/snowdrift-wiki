# Meeting 14

*Wednesday, June 4 2014, 7PM Pacific*  
phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
backup alternate number: 951-262-7373*

live-recording of minutes on [etherpad](https://snowdrift.etherpad.mozilla.org/1)

## Background readings / updates

E-mail threads: Various e-mails were sent out for discussions since last official meeting. Check that you're up on those things, responded if needed. Especially our [legal description](legal-description) which we want to start showing to lawyers ASAP to get some sort of sign-off that we know the basic concept is legally sound enough to move forward.

### Assigned tasks from before

* Aaron: set up Paypal, update homepage, network, recruit, etc., write welcome message to new users, work on presentation / video
* David: *same as last time*: BalancedPayments set up, established user stuff, mod stuff, project application forms live for project intake, if time: more ticketing features and other priority things
* Joe: technical work
* Kim: Standing Rules work
* Mike: WebRTC stuff still: appear.in? meet.jit.si, move project ideas to wiki, keep working on project ideas and issues
* Chad: test Dwolla with LibreJS
* James: *same as last time* Figure out about that possible web-design intern

## Agenda

* Check in with assigned tasks from previous meeting, agenda review
* Technical update
* **Next milestone discussion**
    * Main blocker: legal sign-off on basic structure of the system
        * Our [legal description page](legal-description) is designed to show to lawyers
        * We need to figure out whether there are any legal concerns blocking moving ahead toward operating
        * Essentially: is it merely a matter of finalizing bylaws, IRS status etc. i.e. just going through the process or are there issues with our proposals that we need to address?
        * How do we get legal opinions on this ASAP? Who should we talk to?
    * Setting deadlines, first: Open Source Bridge conference, June 24-27
    * Milestone:
        * project sign-up really?
            * if so, what are the issues? blockers? priorities?
        *  begin more formal fund-drive?
            * if so, what are the issues? blockers? priorities?

* Any other issues / important business

## Next steps assigned

Everyone will work on whatever will help make a better experience for recruiting projects. Continue discussion about details…

## Summary / minutes
    
* Check in, agenda review
    * Mike: https://snowdrift.coop/p/snowdrift/w/meeting14/c/1160
    * Kim: Standing Rules are as done as they can be until we have an internal structure
    * David: tracking actual pledge actions, the pledge formula, that stuff got prioritized over some other items, mother knows a corporate lawyer who might be sympathetic
    * Aaron: All my tasks done from before except video is still in planning stage, considering priorities about reaching out to people versus other work…
* Technical update
* Next milestone discussion
    * MIs legal sign-off on basic structure of the system a blocking concern?
        * What are we actually getting a lawyer to sign off on? What are the legal questions we need answered immediately?
        * Concerns re: whether we can claim that we are absolutely not a money transfer service, whether we need to take steps to clarify this
        * Opinion has been issued that Kiva is not a money transfer service, so we have a pretty clear legal precedent to guide us
        * Our transfer system also raises some issues regarding escrow and whether donations are "to Snowdrift.coop" or "to projects"
        * This impacts whether we pay tax on income, whether patrons can claim their donations as tax-deductible.
        * Is there a qualitative difference if it is legally Snowdrift.coop accepting account deposits as donations?
        * For the moment, assume we are fundamentally an allocation system for the convenience of the patron
        * Decision for now: move on, and focus on fundraising and recruitment, continue making low-level inquiries on legal questions
* Milestone for Open Source Bridge conference, June 24-27
    * Project sign-up and/or begin fund-raising?
    * We need project connections and people testing the site more than we need immediate funding for legal assistance, so sign-up it is
    * Technical work will proceed on this assumption

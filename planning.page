---
title: Planning
categories: project-management, help
...

This is a high-level overview of our upcoming goals. The [team resources] page lists the tools we use.

[team resources]: https://wiki.snowdrift.coop/resources#project-management-development-design

## Roadmap

- [Promote forum for wider participation](https://gitlab.com/groups/snowdrift/-/issues?milestone_title=Discourse+Announce)
- [Recruit official Board](https://gitlab.com/snowdrift/governance/issues/32)
- [Get legal bylaws in place](https://gitlab.com/groups/snowdrift/-/issues?milestone_title=Bylaws+in+place), then follow them in our overall governance
    - Includes solidifying the co-op structure
- Get legal review of other policies / operations for the site
- [Solidify team governance, roles, and working process](https://gitlab.com/groups/snowdrift/-/issues?milestone_title=governance-roles-recruiting)
- Solidify auth / sysadmin / secrets etc. (for a secure, reliable site without a single-point-of-failure)
- [Recruit first outside project(s)](https://gitlab.com/groups/snowdrift/-/issues?milestone_title=1st+outside+project+ready+to+add)
- Have site fully technically ready to [list multiple projects](https://gitlab.com/groups/snowdrift/-/issues?milestone_title=closed+beta) actually doing crowdmatching
- Solid overall site UX
    - all pages on [new visual design](https://gitlab.com/groups/snowdrift/-/issues?milestone_title=alpha+visual+design)
    - visitors easily and effectively understand the core concepts
    - visitors can easily sign-up, pledge, etc. without issues
    - notifications in place
    - promote more widely, get patronage to level worth charging
- [CiviCRM fully working well](https://gitlab.com/snowdrift/ops/issues/4), usable for tracking cases
    - [volunteer form](https://gitlab.com/snowdrift/outreach/issues/28)

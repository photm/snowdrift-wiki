---
title: Honor Code for Projects
categories: governance, project-liaison, honor
...

We encourage patrons to use discretion, have patience, and assume good will from all projects. Patrons should favor the most honorable projects while encouraging others to improve.

The following honor guidelines are separate from our absolute [project requirements](/project-reqs).
**The only strict requirement of the project honor code: Honestly represent your status.** For inclusion at Snowdrift.coop, projects must report their overall standing on items listed below. Each project listing will include this self-report and can be updated if and when any status changes.

The Snowdrift.coop honor system asks projects to collaborate and support one another; to be creative, honest, and responsible; to distribute funding and other resources to team members fairly and equitably; and to strive toward the best service to the global community.

## Favor FLO tools in the creation of all projects

Ideally, use Free/Libre Open Source software in project development. Examples include: music made with FLOSS audio programs, art made with FLOSS image programs, science using FLOSS research programs, and so on. Project websites should be built with FLO frameworks.

Of course, we need Snowdrift.coop only because the best software is not already all FLO. We understand that most people still use proprietary software, and we do not shame anyone over this, but we urge everyone to consider FLOSS options when feasible. As FLO tools get listed at Snowdrift.coop, other projects may use our system to support the tools upon which they rely.

## Make works available through freedom-respecting channels

For example, don't *only* upload works to restricted commercial sites like YouTube or Flickr. If you use those, also have mirrors on freer repositories. See our list of suggestions for [freedom-respecting hosting options](formats-repositories#online-repositories).

## Release source files

Although approved licenses for software require available source code, we want to see source for non-software as well. Furthermore, favor open [non-proprietary formats](formats-repositories#file-formats) for your source files, and, if feasible, include original uncompressed data. For example, rather than only a compressed-format final mix of a music recording, provide lossless audio files of separate tracks.

### Open Data

Research projects should aim to release all data. Given concerns about privacy or other ethical issues, provide a clear explanation about what data will not be released and why.

## Avoid obsolescence

Try to support long-term systems and provide reasonable backward compatibility. Audio/video productions as well as software should be available in formats that do not require cutting-edge technology to run.

## Support services, feature requests, etc.

* Each project has discretion in organizing and prioritizing support services, but should work to provide some form of support for all users regardless of their status as patrons.
* All creative contributions, feature requests, feedback, and support requests should be treated with respect and encouragement regardless of intellectual or technological merit. Requests may be declined, but rejections should be expressed in a manner that welcomes future engagement.
* Hesitate when blaming third-parties for issues. When appropriate, say that you don't know how to fix something or that you believe an issue is outside your control. Avoid conclusions of outside fault unless the issue is fully identified and clear.

## Terms of Use and Privacy Policies

Have truly *readable* and *fair* policies on any external web pages or within software. Consider getting any Terms included on the site [Terms of Service; Didn't Read](http://tosdr.org) and aim for the highest rating.

Make all users aware of any collection of private data. Beyond a sentence in the privacy policy, put real effort toward assuring that users are in fact aware of any private data collection.

### Ideal policies

* *Opt-in* for all sharing of private information with third-parties, except where obviously fundamental to the project's function.
* Automatic data collection should at least offer an opt-out wherever feasible.
* Users should not have to waive essential rights or legal access to due process.
* Projects should provide advanced notification and, ideally, allow for user feedback before any substantial change of policies, including in the case that the project gets transferred to new management / ownership.
* Avoid lock-in. Make it possible for users to download their data in non-proprietary formats.
* Projects should allow users to close any accounts and to fully remove any *private* data.
    * Balance the value of retaining of transparent, accurate records of *public* activity against allowances for privacy and personal control.

## Avoid Service as a Software Substitute (SaaSS)

As described in Richard Stallman's article [*Who does that server really serve?*](https://www.gnu.org/philosophy/who-does-that-server-really-serve.html), SaaSS refers to computing that could be done on a user's own computer but is instead done remotely over the internet on a server that the user does not control. Online services should only involve communication, networking, collaboration, and publication — all things that necessitate connection to online servers.

Whenever possible, permit and encourage users to do their own private computing on their own machines by downloading software they can run locally. Optional and transparent connections between local software and online servers are acceptable when necessary for integration. Consider the aims laid out in the [Franklin Street Statement on Freedom and Network Services](http://autonomo.us/2008/07/14/franklin-street-statement)

## Compatibility and integration

* Avoid reliance on proprietary libraries, plugins, services, etc. to function. Furthermore, do not *encourage* the use of proprietary products even when supporting compatibility as an option.
    * At the very least, accept the burden of justifying any inclusion of or connection to proprietary products and provide information explaining any concerns about non-free elements of the project.
* Aim for full native compatibility with FLO platforms, e.g. make software compatible with GNU/Linux. Furthermore, encourage users to choose FLO platforms and make references to FLO systems at least as prominent as references to proprietary systems.

## Connection to third-party domains

Many websites connect to third-party domains to load images, JavaScript, analysis tools, and more. This allows prominent third-party hosts to track users across all the sites that make connections to them and so may present a privacy concern. It also reduces a project's control over their own infrastructure.

When feasible, host your own images, JavaScript, fonts, and other content. Work to make your website still function even for a user who blocks third-party data requests. At the least, be transparent about third-party hosting you use.

Although it's ideal to function without any third-party requests, some are more acceptable than others. Acceptable-enough third-party connections ise (A) FLO resources (this avoids lock-in as the resources *could* be moved to local hosting at a later time) and (B) clear and respectable privacy policies. For example, Google's font service uses only FLO fonts, allows you to download them, and has an acceptable and clear [privacy policy](https://developers.google.com/fonts/faq#Privacy). If Google's policy changes or the service is removed, it is easy to adapt and get the fonts for hosting otherwise. While we do *not* recommend Google fonts over self-hosting, it represents an *acceptable* third-party service. By contrast, third-party resources that are proprietary and have worse privacy policies are not okay.

### Site usage analysis

If you would like to perform more detailed analysis on visits to your site, **use self-hosted analysis tools that you control directly**. FLO options include [Matomo](https://matomo.org) (formerly Piwik), [Fathom](https://usefathom.com/) and [OpenWebAnalytics](http://www.openwebanalytics.com/). Make sure to be transparent about data collected, see above about [*privacy policy*](#on-terms-of-use-and-privacy-policies).

### Connections to social-networking and others

* Make project news, discussion forums, and support materials as open as possible. Avoid *reliance* on proprietary platforms like Facebook/Twitter/etc. This helps to avoid the "walled garden" effect on community participation and assures inclusion of those users who do not use these proprietary services.
* Using any social networking services to reach people on those platforms is otherwise acceptable, but avoid their JavaScript buttons which do not give you control and which track users without their consent
    * **Simplest solution: use a normal HTML link** to go directly to your project's page at the site in question. As mentioned above, you should also host your own copy of the logos from external sites.
    * For a slightly more involved option, consider: [Social link the right way](http://yannesposito.com/Scratch/en/blog/Social-link-the-right-way/)
    * Admittedly, some people may desire tracking and integration. We consider it acceptable to use an *opt-in* JavaScript setup; see [Heise's jQuery Plug-In socialshareprivacy](http://www.h-online.com/features/Two-clicks-for-more-privacy-1783256.html)
    * Similar adjustments should be made when linking to all sorts of other services.^[For example, Paypal's donation buttons come with tracking features including a 1-pixel image file. You can instead self-host the donate button image and remove all but the non-essential parts of the HTML code. The result will look like this (replace the two code bits with the appropriate codes generated by Paypal): `<form target="_top" method="post" action="https://www.paypal.com/cgi-bin/webscr" abineguid="[LONGCODE]"><input type="hidden" value="_s-xclick" name="cmd">
<input type="hidden" value="[SHORTERCODE]" name="hosted_button_id">
<input type="image" alt="Donate via PayPal" name="submit" src="/assets/external/paypal-donate.gif">
</form>`]

## Third-party advertisements

![](/assets/nina/MimiEunice_59-640x199.png)

For ads on external websites, within software, or placed in artistic productions, follow these guidelines:

* BARE MINIMUM: Ads are distinctly identifiable as ads and follow the rest of the guidelines for [Adblock Plus's acceptable ads](https://adblockplus.org/en/acceptable-ads#criteria), i.e. not obtrusive, distracting, or misleading. Ad revenue must be reported fully as part of our [transparency requirements](transparency)
* BETTER: Ads only reference products that the project team feels are ethical, high quality, presented in honest and honorable ways, relevant to the topic of the project, and do not promote behavior that is opposed to free/libre/open ideals
    * The site managers should have control to accept or reject particular ads and control the placement
    * No embedded third-party trackers
    * Avoid sending visitors to download sites that do not meet these standards
    * Report your use of any kickback links (e.g. as a partner with Amazon.com) and consider FLO reference sites like Wikipedia as an alternative. Also, use transparent links and not ones that actually pass through a separate third-party tracking system.
* BEST: no third-party ads of any kind

## Follow universal standards and accessibility guidelines

Use open standards for design and technology wherever feasible.

* Websites should avoid browser-specific features.
* Make sure that sites can at least *function* without JavaScript and in text-only browsers.
* Provide secure connections by default.
    * Note: FLO projects can get SSL certificates from [Globalsign](https://www.globalsign.com/en/ssl/ssl-open-source/) at no charge!
* Follow other guidelines for accessibility for those with special needs, such as [Section 508](http://www.access-board.gov/guidelines-and-standards/communications-and-it/about-the-section-508-standards/section-508-standards), the US Government's standard for information access for those with disabilities.

## Build with ethical design and public interest in mind

Follow the ideals of [Ethical Design](https://ind.ie/ethical-design/) and [Time Well Spent](http://www.timewellspent.io/designers/) and otherwise ensure that your product actually improves the world and its users rather than wasting their time or manipulating them into behaviors that do not serve their long-term interests.

## Relation to other projects

### Aim for constructive collaboration

Avoid a not-invented-here attitude. Assist other projects to the best of your ability. Partner with related projects to maximize the impact of your work.

**Be sensitive about forking**. Fork a project only with strong justification and explain the reasoning publicly. If you have concerns about the direction of an existing project, communicate them clearly, give the project a reasonable amount of time to respond, and seek a win-win solution. If this fails to produce results, consult with the community about wider support for a fork. Forks most often result in the demise of the branch with less community support. Always contribute upstream as much as possible and design your work to maximize flexible use by other projects.

### Compete constructively

Promote and amplify your virtues and differences, but refrain from excessive criticism of other honorable free/libre/open projects. Avoid anti-competitive behaviors including decisions that lock users into your product.

### Honestly represent your relation to other projects

Respect the terms of licenses. Work to give public credit to any work upon which you are building. Report the extent of your contributions fairly and honestly. For more in-depth guidelines, consider the Creative Commons [Public Domain Guidelines](http://wiki.creativecommons.org/Public_Domain_Guidelines) to assure full respect for existing resources.

#### Avoid proprietary partnerships or side-projects

Given the challenges that Snowdrift.coop works to address, many FLO projects get sponsorship and support from related proprietary projects. Some even make their own proprietary products separately from their FLO work. We accept such FLO projects if the distinctions are clear enough (distinct names, websites, etc.), the details are published transparently, and the fully-FLO products remain independently functional.

## Team governance and operations

###  Use funds efficiently and effectively

Funding should focus on serving the project's core mission of enhancing its FLO products.

Hire as many quality team members as can be put to good use and as funds will permit while aiming to provide workers a [living wage](https://en.wikipedia.org/wiki/Living_wage).[^compensation]

[^compensation]: Compensation rates for team members can be controversial and complex. Our ideal is a decent living for all.

    Of course, a project with limited funding must decide whether to support a larger team only part-time (thus requiring them to find additional outside income, either through unrelated jobs or from other FLO projects) or to provide a higher level of support to a smaller team. While we encourage paying a living wage, projects with limited income must make tough choices.

    On the other side, projects may choose to pay higher competitive market rates in order to recruit and retain the best teams who might otherwise work on proprietary projects. Where feasible, we encourage projects to find teams with lower cost of living in order to make the most of limited resources. As a general guideline, projects should recruit the best teams by emphasizing a decent living, autonomy, challenge, and social purpose instead of focusing on the highest pay or other unnecessary luxuries.

Appropriate uses of funds:

- Payments to team members for ongoing work
- Covering necessary expenses, including incidental bandwidth/hosting costs ^[Costs considered *incidental* are those that are only a small fraction of overall development expenses. Where feasible, pass on the costs of expensive non-rivalrous resources like hosting of high-bandwidth items (such as large quantities of streaming HD video). Such non-rivalrous resources *should* be paid for by those who use them. Keeping these costs direct encourages a more honest economic market, better transparency, and conservative use of limited resources.]
- Saving funds in accounts budgeted for future costs
- Donations to upstream FLO projects [^upstream]

[^upstream]: Any project that is directly related or has significantly contributed to your development can be considered 'upstream'. For example, Snowdrift.coop is an upstream project for others on the site because we provide a supporting service.

### Aim for consensus in decision-making

Among team members and within the larger community, look for win-win solutions and real consensus. Include everyone who shares the project's basic values and goals and shows willingness to put in the time and effort to contribute productively.

#### Pursue a vision

While we value teamwork and consensus, sometimes a project's vision does not match popular opinion. When you believe in a vision, make the goals clear and recruit a team and community who will support you. Share the vision as early as possible to avoid the appearance of arbitrarily rejecting certain people's views.

### Consider cooperative structure

Cooperative organizations can have various structures. Any project is welcome to adapt the technical tools and governing bylaws used by Snowdrift.coop.

### Consider non-profit structure

Non-profit structures allow an organization to focus on a social mission, although there are also for-profit structures worth exploring such as the [Benefit Corporation](https://en.wikipedia.org/wiki/Benefit_corporation). Of course, a project's actions over time and accountability to the community matter more than their legal structure. **Have a social mission** that involves a net positive impact on the world.

### Avoid conflict-of-interest

Follow good conflict-of-interest guidelines and ask team members to take action to not only reveal but also reduce or eliminate any conflict of interest between their involvement in the project and their involvement in any other outside activities.

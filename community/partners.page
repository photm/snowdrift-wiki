---
title: Snowdrift.coop Partners
categories: community
...

## Connected partners

### [Open Source Initiative](http://opensource.org/)

The primary open source advocacy group, originally formed to promote collaboration on software in a way more business-friendly than the ethical/political focus of FSF. Today, the OSI is less opposed to the FSF than they were historically and most directors are FSF members. They continue to focus on adoption of open source as widely as possible, whether within proprietary software businesses or elsewhere. As of late 2015, Snowdrift.coop is a formal [OSI affiliate and incubator project](https://snowdrift.coop/p/snowdrift/blog/osi-partnership)

### [QuestionCopyright.org](http://questioncopyright.org) (QCO)

A FLO culture and public-domain advocacy and educational organization. QCO endorses our efforts to help artist make a living without relying on copyright restrictions. The QCO Artist-in-Residence, Nina Paley, created the Mimi and Eunice characters that we adapted for our own use. QCO Founder and director Karl Fogel has also been an informal but strong supporter, giving feedback and encouragement since the earliest days of Snowdrift.coop.

### [Open Invention Network](https://www.openinventionnetwork.com)

Snowdrift.coop has agreed to the OIN licensing pool stating that we will cross-license any patents we ever hold (we anticipate having none though) to other members of the OIN. The purpose of the OIN is to assure that patents won't be used aggressively against Linux and the Linux ecosystem. While we endorse that goal, we also wish to see patents never used against any FLO projects at all, so we also encourage consideration of the [Defensive Patent License](http://defensivepatentlicense.org/) and other efforts to limit the threat of patents against innovation and the public commons.

### [Copyfree Initiative](http://copyfree.org/)

The CI advocates specifically for those FLO licenses they consider "Copyfree" which place no restrictions on materials outside of the licensed work itself, not even the copyleft restrictions on maintaining the license for derivatives. Copyfree licenses range from effective public-domain dedications to licenses that include liability waivers, requirements to include the license, and protection from false claims of endorsement or attribution. Copyfree licenses are maximally compatible with all other works. See the full definition at <http://copyfree.org/standard>. Although we use copy*left* licenses ourselves at Snowdrift.coop, we respect projects freedom to choose their preferred FLO licenses.


---
title: The Snowdrift.coop Funding Mechanism: Crowdmatching
categories: communications
...

*The main Snowdrift.coop site has basic introductions to crowdmatching. This page provides a more thorough explanation.*

Snowdrift.coop funds public goods through *crowdmatching* where patrons donate together with everyone matching each other. For each new patron who joins, all patrons give a little extra.

The need for crowdmatching comes from addressing the [Snowdrift Dilemma](/about/snowdrift-dilemma) faced by public goods.^[Some readers may be familiar with Paul Harrison's proposed [Rational Street Performer Protocol](http://www.logarithmic.net/pfh/rspp), which also involves many-to-many matching pledges. Snowdrift.coop is the first platform to implement this type of idea. Compared to the RSPP, Snowdrift.coop reduces some awkwardness it had, applies it to subscription-style pledging, and adds a broader platform context to provide the holistic that we feel the system needs to truly succeed.]

Crowdmatching is flexible (rather than all-or-nothing), and minimizes individual risk and burden while maximizing collective impact.

## The pledge process

1. Patron adds a payment account
2. Patron visits a project page and clicks the pledge button.
3. Once a month, the patron numbers are calculated in a "crowdmatch" event
    - each patron's balance is updated with the new donation(s)
    - Whenever a patron's balance surpasses the level that can be charged while keeping processing fees under than 10%, we instruct the processor to charge their credit card and distribute their donations to the projects they support.

## 1/10 cent starting match level

We're starting with a base match level of 0.1¢. That's 1¢ per 10 patrons or $1 per 1,000 patrons.

## Each patron's donations grow linearly

The donation from any one patron increases *linearly* with the crowd size.

![](/assets/mechanism/pledgechart1.png)

## Funding grows quadratically with more patrons

A linear increase in number of patrons multiplied by a linear increase in each patron's donation means a *quadratic* increase in total donations.

![](/assets/mechanism/pledgechart2.png)

Examples:

* 200 patrons × 20¢ each = $40 per month for the project
* 1,000 patrons × $1 each = $1,000 per month for the project
* 10,000 patrons × $10 each = $100,000 per month for the project

## Budget limits

Ideally, crowdmatching will grow until projects are funded enough to support their *full* potential (not just short-term goals). We hope as many patrons as possible stay in as pledge values increase, but to provide a control point, patrons can set a budget limit.

Under no circumstance will a user be charged more than their budget limit in any given month. If a user's pledges surpasse their budget limit, the system will suspend the pledge that rose too high.

We're starting with a $10 monthly budget default. We plan to add the ability for each user to adjust their budget. And we hope that by the time patrons reach the budget limit, they will be so thrilled with the impact of collective donations, that they will choose to increase their budget.

If a user wishes to reinstate a suspended pledge without an overall budget increase, they could drop other pledges to other projects.

---


## Benefits of Crowdmatching

The core purpose of Crowdmatching is to address the [Snowdrift Dilemma](snowdrift-dilemma) by reducing the incentives that delay donating. Beyond that, it has other benefits.

### Flexibility with practical limits

Cooperation and negotiations require flexibility from everyone involved. As seen in game theory scenarios, **a known end or fixed maximum can lead to reduced cooperation**. Fixed amounts of funds mean zero-sum games. Even with matching funds, if the available budgets for matching are strictly fixed, then potential donors may recognize that as long as *enough* people pledge, the total possible matching will be claimed, and so they can hope that others will take care of it.

Snowdrift.coop uses budget caps to give patrons control. But we promote non-zero-sum thinking by encouraging patrons to increase their budgets as possible. When a project gets very popular and gains many patrons, we hope the project will deliver such remarkable results from the increased support that it convinces patrons of the value of continuing to donate beyond even the levels they initially considered.

Patrons cannot easily *predetermine* what donation level is "fair". Human beings do not judge [value and price](markets-and-prices) objectively. Anchor values (such as the "suggested price" on products) make a "reduced" sale price seem great. Consider also how auctions work: people may plan to bid only a certain amount, but our perception of value changes when we see that others are willing to bid more. Complexity only increases when it comes to funding future work where we do not already experience the value of the results.

Crowdmatching avoids arbitrary thresholds seen in many one-off crowdfunding campaigns. We aim to bring out the best from everyone, all contributing their share. As patrons see the value of working together, they will realize the need to be flexible themselves in order to build together the world we all want. We hope that the natural market forces within our system will maximize funding and yet stabilize at sustainable levels due to the system's inherent **[practical limits](limits)**.

#### Testing and natural patterns within the system

Once the site has more advanced functioning (with actual graphs and data generated by the test users), we can tweak things as needed. Natural patterns will emerge regarding how patrons pledge and adjust pledges across different projects and over time along with project progress and growth.

### Constructive competition

Projects at Snowdrift.coop still compete for funds. When patrons do stick to a strict budget, they may need to prioritize among the projects they would like to support. That's the way the market goes sometimes, and it makes sense for the community to get behind the most promising projects. At the same time, the market itself can grow and serve more projects as the platform attracts more total patrons.

### Project growth and stability

**Snowdrift.coop aims to provide relatively reliable funding streams that allow projects to plan development more effectively.** Growing income enables hiring more team members, creating operating reserves, and/or donating to fund upstream projects.

An adequately funded project could announce its status. Then, pledge levels will naturally stabilize as patrons choose to focus on other projects with unmet needs.

### Economic Democracy

Crowdmatching enables the community to work together in a bottom-up fashion to help maximize the overall funding while [democratically](democracy) routing it to deserving projects. Our [basic requirements](/project-reqs) and honor reporting along with user reviews, history, and other design features will help patrons trust the projects in the system and identify the most worthy ones to support.

### Reliability, Accountability, Trust

**With ongoing regular payouts, patrons have time to evaluate each project's progress and alter their pledges.** That gives projects extra incentive to consider the needs and desires of their patrons.

---

## Potential future features and options

We may offer per-user or even per-project adjustments in the future once the system is working fully. However, this introduces a wide range of complex issues which only make sense to consider once the basic crowdmatching concept is working and widely understood.

### Higher match levels from wealthier patrons?

While we want (for the sake of project success) to have wealthier donors give extra, we also value the equalized influence that comes from everyone donating at the same level.

We also don't want overly-flexible match levels to to undermine the network effect. If a patron can start at a high level and reduce that as the project grows, they aren't actually matching others but are actually doing the opposite (giving less as more people join). It only makes sense to *reduce* donations as others join *after* a project is *fully* funded (a point we hope to eventually reach but which is different from the challenges FLO public goods face today).

We originally proposed a "share" method where instead of changing the base match, each patron could choose to double, triple (or more) their pledge to any particular project. We wanted to encourage that generosity with some additional matching. To do that without letting one generous patron manipulate the donations of everyone else, we proposed a partial match that tapered off for these increased pledges. While our original model had merit, it presented serious trade-offs, most significantly in how difficult it was to explain. But if you're curious, see our [old share-value formula](/archives/communications/formula).

### Varying situations among projects

Some projects inherently have different financial needs and different sizes of audience. We could offer some ways for projects to set a different base match level. But crowdmatching may just not work well for all projects. For projects that serve other projects (instead of the end-user general public), we encourage the dependent projects to contribute their income upstream.

Some projects may be more focused on institutional audiences. We will consider in the future whether to add some special institutional patron classification with appropriate settings.

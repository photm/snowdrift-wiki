---
title: Snowdrift.coop Intro for Artists
toc: false
categories: communications, intro
...

## Your focus

You want to bring to world new meaningful and quality art, music, literature or other media. You know that you are building on the works of others that have come before you (whether consciously or simply as cultural influences) and that beyond your specific works, your legacy will be in the myriad ways you influence and inspire others. You want to make your work available to the widest possible audience.

## Your problems

Despite the romantic view of the starving artist, you'd really rather not starve nor have to sell-out by doing a meaningless day job or by compromising your artistic values. You want to focus on communicating with your audience rather than on marketing your works.

## How we can help you

Snowdrift.coop believes in cultural freedoms. Everyone should have access and freedom to share and to add their contributions. We help artists make a living while upholding these ideals.

Our monthly funding builds as you gain interested supporters, so you can cover your costs and even earn a fair living doing what you love. Instead of the stress and cost of one-time fund-drives or the per-release sales or donations that emphasize quantity over quality, we want you to focus making the most meaningful art. In our system, you retain autonomy to follow your creative muse while still getting feedback from your supporters. We do not put a strict price on things or quantify everything. Art is priceless. We just need to assure that artists can eat, be healthy, and prosper.

We require Free/Libre/Open licenses, but that does not remove your core moral rights. The license we use ourselves, the Creative Commons Attribution/Share-Alike license (CC-BY-SA), protects authors' rights to be credited for their work. As a copyleft license, it also requires that derivative users make their work available under the same terms, thus preventing co-option by proprietary businesses. These requirements do not get in the way of basic creative freedom the way other copyright restrictions do. Those who wish to waive even these requirements with more permissive licenses may still do so.

With our system, you can make a living while maintaining your artistic freedom and fully respecting the freedom of others.

## How you can help us

Our site could use illustrations, video explanations, and improved design! Perhaps you could help create comics or graphics to help us explain who we are and what we do.

If you are a writer, we welcome edits to our writings and suggestions for new slogans or ways of explaining and expressing our mission!

If you do web design or HTML/CSS/JS, we could use help with our site [design](https://gitlab.com/snowdrift/design)!

We also need help with outreach to artists, addressing concerns that they may have, and getting them signed up on the site.
